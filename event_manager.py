import json
import time
import socket
import select
import signal
from uuid import uuid4
from threading import Lock, Thread
from thread_pool import ThreadPool

import utils


class EventManager(object):
    def __init__(self, port, backlog=5, num_threads=10):
        """

        :param port: server port number
        :param backlog: number of pending connections in the queue
        :param num_threads: number of worker threads in the thread pool
        """
        # Class members initialization
        self.lock = Lock()
        self.subscriber_map = {}
        self.topic_map = {}
        self.clients = {}

        # Construct worker pool (by default 10 worker thread)
        self.worker_pool = ThreadPool(num_threads)

        # Construct a TCP server
        self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.server.bind(('', port))
        self.server.listen(backlog)

        # Signal handler
        signal.signal(signal.SIGINT, self.sig_handler)

    @staticmethod
    def generate_unique_id():
        """
        Create a UUID from a string of 32 hexadecimal digits
        :return:
        """
        return uuid4().hex

    def sig_handler(self, signum, frame):
        """
        Signal interrupt handler
        :param signum:
        :param frame:
        :return:
        """
        for v in self.clients.values():
            if v is not None:
                v.close()

        self.server.close()

    def scan_and_send(self):
        """
        Send updated message or article to subscribers
        :return:
        """
        while True:
            # this thread function runs every half second
            time.sleep(0.5)

            self.lock.acquire()

            # Loop through the subscriber_map
            for topic, subs in self.subscriber_map.iteritems():

                # Find the number of the new messages from this topic
                last_index = self.topic_map[topic]['lastIndex']
                topic_length = last_index + len(self.topic_map[topic]['msg'])

                # New message to each subscriber who have subscribed this topic
                for sub, v in subs.iteritems():
                    if topic_length > v:
                        try:

                            # client socket fd
                            client_sock = self.clients[sub]
                            if not client_sock:
                                continue

                            # Message formulation based on the last index and the number of message
                            # that has previous received.
                            message = {"verb": "notify", "topic": topic,
                                       "msg": self.topic_map[topic]['msg'][v - last_index:]}
                            self.worker_pool.add_task(self.send, client_sock, sub, topic, topic_length, message)
                        except KeyError:
                            pass

            # Release lock
            self.lock.release()
            self.shrink_messages()

    def shrink_messages(self):
        """
        Minimize the previous message for each topics.
        :return:
        """
        self.lock.acquire()
        for topic in self.topic_map:
            if self.subscriber_map[topic]:
                idx = min(_ for _ in self.subscriber_map[topic].values())

                new_idx = idx - self.topic_map[topic]['lastIndex']
                self.topic_map[topic]['lastIndex'] = idx
                self.topic_map[topic]['msg'] = self.topic_map[topic]['msg'][new_idx:]
        self.lock.release()

    def send(self, client_sock, _id, topic, idx, message):
        """
        Send message to the connected client(Worker thread's job).
        :param client_sock:
        :param _id:
        :param topic:
        :param idx:
        :param message:
        :return:
        """
        try:
            m = json.dumps(message)

            client_sock.settimeout(400)
            utils.send(client_sock, m)
        except socket.timeout:
            pass
        except IOError:
            self.clients[_id] = None

        else:
            if topic is not None:
                self.lock.acquire()
                self.subscriber_map[topic][_id] = max(idx, self.subscriber_map[topic][_id])
                self.lock.release()

    def send_advertise(self, client_sock, _id, message):
        try:
            m = json.dumps(message)
            client_sock.settimeout(400)
            utils.send(client_sock, m)
        except socket.timeout:
            pass
        except IOError:
            self.clients[_id] = None

    def run_server(self):
        """
        Server handle all the incoming client socket for both
        Publisher and Subscriber. This process also extract the
        request message for connection ID assignment, publish
        request, subscribe request and un-subscribe request.
        :return:
        """
        inputs = {self.server}

        while True:
            try:
                # The first three arguments are sequences
                # reading, writing and exception condition
                inputs_ready, output_ready, exception_ready = select.select(inputs, [], [])
            except (select.error, socket.error):
                break

            for s in inputs_ready:
                if s == self.server:
                    # monitoring the client socket connection
                    client_sock, address = self.server.accept()
                    # put the client socket to the input channel list
                    inputs.add(client_sock)
                else:
                    # ************************ Data retrieving from client ********************
                    try:
                        # read data from the current client
                        data = utils.receive(s)
                    except socket.error:
                        # Remove the connected client from the input channel list
                        # if there is any socket process error
                        inputs.remove(s)
                        continue

                    try:
                        # Deconding JSON
                        d = json.loads(data)
                    except (json.JSONDecoder, ValueError):
                        continue

                    # ************************ Pub Sub Message Handling ***********************  
                    verb = d.get('verb')
                    topic = d.get('topic')

                    # Check client ID
                    _id = d.get('_id')
                    if _id is None:
                        _id = self.generate_unique_id()

                    # Store the socket to the client list
                    if _id not in self.clients or self.clients[_id] != s:
                        self.clients[_id] = s

                    self.lock.acquire()

                    if verb == "advertise":
                        if topic not in self.topic_map:
                            self.topic_map[topic] = {"msg": [], "lastIndex": 0}
                            self.subscriber_map[topic] = {}

                        msg = {"ok": True, "verb": "advertise", "topic": topic}
                        for ii, c in self.clients.iteritems():
                            if c == s:
                                continue
                            self.worker_pool.add_task(self.send_advertise, c, ii, msg)

                    if verb == 'connect':
                        msg = {"ok": True, "_id": _id}
                        self.worker_pool.add_task(self.send, s, _id, None, 0, msg)

                    # Publish request
                    if verb == 'publish':
                        message = d.get('msg')
                        if topic in self.topic_map:
                            # IF the topic is already in the topic_map
                            # add the article to the existing topic
                            self.topic_map[topic]["msg"].append(message)
                        else:
                            # If the topic is not in the topic_map,
                            # We create a new topic and put the very first message into
                            # the topic_map
                            self.topic_map[topic] = {"msg": [message], "lastIndex": 0}
                            self.subscriber_map[topic] = {}

                    # Subscribe request
                    elif verb == 'subscribe':
                        try:
                            # Add topic to the subscriber map
                            l = self.topic_map[topic]["lastIndex"] + len(self.topic_map[topic]["msg"])
                            self.subscriber_map[topic][_id] = l
                        except KeyError:
                            msg = {"ok": False, "error_msg": "No such topic : %s" % topic}
                        else:
                            msg = {"ok": True, "_id": _id}

                        self.worker_pool.add_task(self.send, s, _id, None, 0, msg)

                    # Un-subscribe request
                    elif verb == 'un_subscribe':
                        # if the topic is in the subscriber map
                        # remove the id from the topic
                        if topic in self.subscriber_map and _id in self.subscriber_map[topic]:
                            del self.subscriber_map[topic][_id]

                    # Release lock
                    self.lock.release()

            # This function runs every 10ms 
            time.sleep(0.01)


# Main function
if __name__ == '__main__':
    # Declare an EventManager object
    e = EventManager(8000, 10)

    # Create a thread object that runs scan_and_send process
    t = Thread(target=e.scan_and_send)
    t.setDaemon(True)
    t.start()

    # Run server system
    e.run_server()
