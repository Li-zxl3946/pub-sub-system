import socket
import json
import utils
import sys
import select
from threading import Thread
from pathlib import Path
import pickle


class PubSub(object):
    # ********************************************************************
    # * Function: __init__
    # *
    # * Params:
    # *  @ serverIP: server ip address
    # *  @ port: server port number
    # *  @ fileName: file name that contains the user ID in hex
    # *
    # * Description:Defalut constructor of PubSub
    # * 
    # ********************************************************************
    def __init__(self, serverIP, serverPort, fileName):
        try:
            # Construct a tpc client socket
            self.clientSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            print "Socket successfully created"
        except socket.error as err:
            print "socket creation failed with error %s" % (err)

        self.fileName = fileName
        self.idExists = False
        d = {'verb': 'connect'}

        # Look into the given file path and check for the exsiting user id
        myFile = Path(self.fileName)
        if myFile.is_file():
            f = open(self.fileName, "r")
            contents = f.read()
            d['_id'] = contents
            self._id = contents
            self.idExists = True

        # Connect client to the running PubSub server
        self.clientSocket.connect((serverIP, int(serverPort)))
        utils.send(self.clientSocket, json.dumps(d))

        # Loaded the previous subscribed Topics
        persistSubscribedTopicsFile = Path(self.fileName + '.pkl')
        if persistSubscribedTopicsFile.is_file():
            pkl_file = open(self.fileName + '.pkl', 'rb')
            self.subscribedTopics = pickle.load(pkl_file)
            pkl_file.close()
        else:
            self.subscribedTopics = {}

        t = Thread(target=self.listenPublishedEvents)
        t.setDaemon(True)
        t.start()

    # ********************************************************************
    # * Function: subscribe
    # *
    # * Params:
    # *  @ topic: given topic
    # *  @ callback_function: Run this function if there is any new feed
    # *                       Messages from the event manager
    # *
    # * Description:Send subscribe request to the even manager
    # * 
    # ********************************************************************
    def subscribe(self, topic, callback_function):
        # build a formatted "subscribe" string and send request.
        d = {'verb': 'subscribe', 'topic': topic, '_id': self._id}
        utils.send(self.clientSocket, json.dumps(d))
        self.subscribedTopics[topic] = callback_function

        # save the subscribed topic to the pkl file.
        persistSubscribedTopicsFile = open(self.fileName + ".pkl", 'wb')
        # Pickle dictionary using protocol 0.
        pickle.dump(self.subscribedTopics, persistSubscribedTopicsFile)
        persistSubscribedTopicsFile.close()

    # ********************************************************************
    # * Function: unsubscribe
    # *
    # * Params:
    # *  @ topic: given topic
    # *
    # * Description:Send un-subscribe request for a topic to the even manager
    # * 
    # ********************************************************************
    def unsubscribe(self, topic):
        # build a formatted "un-subscribe" string and send request.
        d = {'verb': 'un_subscribe', 'topic': topic, '_id': self._id}
        utils.send(self.clientSocket, json.dumps(d))

        # remove topics from the local subscribed topic list
        if topic in self.subscribedTopics:
            del self.subscribedTopics[topic]
            persistSubscribedTopicsFile = open(self.fileName + ".pkl", 'wb')
            # Pickle dictionary using protocol 0.
            pickle.dump(self.subscribedTopics, persistSubscribedTopicsFile)
            persistSubscribedTopicsFile.close()
            return "Unsubscribed successfully!"
        else:
            return "Topic not subscribed yet!"

    # ********************************************************************
    # * Function: listSubscribedTopics
    # *
    # * Params:
    # *  @ None
    # *
    # * Description:Display a list of the subscribed topics
    # * 
    # ********************************************************************
    def listSubscribedTopics(self):
        if len(self.subscribedTopics) == 0:
            return "No topics subscribed!"
        else:
            return self.subscribedTopics

    # ********************************************************************
    # * Function: listSubscribedTopics
    # *
    # * Params:
    # *  @ topic: given topic
    # *  @ msg: New message or new article
    # *
    # * Description: publish new message and send the request to the event
    # *              manager.
    # * 
    # ******************************************************************** 
    def publish(self, topic, msg):
        d = {'verb': 'publish', 'msg': msg, "topic": topic, '_id': self._id}
        utils.send(self.clientSocket, json.dumps(d))
        return "Published successfully!"

    def advertise(self, topic):
        d = {"verb": "advertise", "topic": topic}
        return utils.send(self.clientSocket, json.dumps(d))

    # ********************************************************************
    # * Function: listenPublishedEvents
    # *
    # * Params:
    # *  @ None
    # *
    # * Description: Monitoring system that actively capture tcp message
    # *              from the event manager.
    # * 
    # ******************************************************************** 
    def listenPublishedEvents(self):
        inputs = {self.clientSocket}

        # receive data from the server
        while True:
            try:
                # The first three arguments are sequences of "Waitable objects"
                # reading, writing and exception condition
                inputs_ready, output_ready, exception_ready = select.select(inputs, [], [])
            except (select.error, socket.error):
                break

            for s in inputs_ready:
                try:
                    # recevie data from server
                    data = utils.receive(s)
                except socket.error:
                    inputs.remove(s)
                    continue

                try:
                    # Deconding JSON
                    d = json.loads(data)
                except (json.JSONDecoder, ValueError):
                    continue

                verb = d.get('verb')
                _id = d.get('_id')

                if _id is not None and not self.idExists:
                    f = open(self.fileName, "w+")
                    f.write(_id)
                    f.close()
                    self.idExists = True
                    self._id = _id

                topic = d.get('topic')

                # if the topic has new message, callback function display the new content
                if verb == "advertise":
                    print "New Advertised topic ", topic

                elif verb is not None:
                    messages = d.get('msg')
                    print messages
                    for message in messages:
                        self.subscribedTopics[topic](topic, str(message.encode("ascii")))


# ********************************************************************
# * Function: callback1
# *
# * Params:
# *  @ topic: given topic
# *  @ msg: given message
# *
# * Description: Callback function that use to display message for a topic
# * 
# ******************************************************************** 
def callback1(topic, msg):
    print("\n\n")
    print("[INFO] Event Manager Notification")
    print("[INFO] Recevied Topic: " + topic)
    print("[INFO] Received Message: " + msg)
    return


# ********************************************************************
# * Function: api
# *
# * Params:
# *  @ None
# *
# * Description: Help menu
# * 
# ******************************************************************** 
def api():
    print "Run from the following commands:"
    print "pubSub.publish(topic, msg)"
    print "pubSub.subscribe(topic,callback_function)"
    print "pubSub.unsubscribe(topic)"
    print "pubSub.listSubscribedTopics()"


def heading():
    print "****************************************************"
    print "*"
    print "* Welcome to the Pub/Sub Application"
    print "*"
    print "* Command List"
    print "* [0] -Display the guideline heading"
    print "* [1] -Advertise a topic"
    print "* [2] -Publish a message with a given topic"
    print "* [3] -Subscribe to a topic"
    print "* [4] -Unsubscribe a topic"
    print "* [5] -Display a list of subscribed topic(s)"
    print "* [6] -Go back to the main menu at anytime"
    print "* [7] -Exit the program"
    print "****************************************************"


def getAdvertiseParam():
    global pubSubClient
    topic = raw_input("\nPlease enter a new topic for advertising:")
    if topic == "6":
        print "[INFO] Return to the main menu.\n"
    else:
        pubSubClient.advertise(topic)
        print "[INFO] Advertised successfully!\n"


def getPublishParam():
    global pubSubClient
    topic = raw_input("\nPlease enter a topic for publishing:")
    if topic == "6":
        print "[INFO] Return to the main menu.\n"
    else:
        msg = raw_input("\nPlease enter the message content for publishing:")
        if msg == "6":
            print "[INFO] Return to the main menu.\n"
        else:
            pubSubClient.publish(topic, msg)
            print "[INFO] Published successfully!\n"


def getSubscribeParam():
    global pubSubClient
    topic = raw_input("\nPlease enter a topic for Subscribing:")
    if topic == "6":
        print "[INFO] Return to the main menu.\n"
    else:
        pubSubClient.subscribe(topic, callback1)
        print "[INFO] Subscribed successfully!\n"


def getUnsubscribeParam():
    global pubSubClient
    topic = raw_input("\nPlease enter a topic for Un-subscribing:")
    if topic == "6":
        print "[INFO] Return to the main menu.\n"
    else:
        pubSubClient.unsubscribe(topic)


if __name__ == '__main__':
    global pubSubClient
    # Takes the pipe line argument
    pubSubClient = PubSub(sys.argv[1], sys.argv[2], sys.argv[3])
    heading()
    while True:
        cmd = raw_input("Please enter a number correspoding to PubSub commands:")

        if cmd == "0":
            heading()
        elif cmd == "1":
            getAdvertiseParam()
        elif cmd == "2":
            getPublishParam()
        elif cmd == "3":
            getSubscribeParam()
        elif cmd == "4":
            getUnsubscribeParam()
        elif cmd == "5":
            pubSubClient.listSubscribedTopics()
        elif cmd == "6":
            print "[INFO] Return to the main menu.\n"
        elif cmd == "7":
            print "[INFO] User terminated the program.\n"
            break
        elif cmd == "":
            continue
        else:
            print "[ERROR] INVALID USER INPUT!"

        print "****************************************************"

        # close the connection
        # pubSub.socket.close()
