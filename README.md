CSCI 652 Group Project 1.1 Publish/Subscribe System Implementation

Description: 
Our group has built a simple PubSub server and client applicaiton for this class project in a topic based model. Event manager of the PubSub needed to processes all the incoming various requests from the pubsub clients in order to maintain the update of the message/content in the database. This system also notified the messages information to the subscribers if they had follow the corresponding topic. On the other end of the client application, we have generated a pubSub class that allow the user to use python command to mannually send subscribe, publish, advertis and un-subscribe request to the event manager. The publisher needed to defined the topic/catergory with the published message. All subscribers to a topic should have received the same content/updated message. 

Application Spec:
Platform: any operating system that runs Python
Programming Language: Python
Supporting Version: 2.7 or newer version
Addtional Library: PathLib

General Test Setup:
1. This Test requires three running operating system/Virtual machines within the same subnet.
2. System 1 execute python command to host the even manager: Python event_manger.py
3. System 2 and system 3 need to provide the IP address of the host system and the port number of the connection. pubSub class will also require the a text file name to store the user ID for the event manager to keep tracking the exsiting users in the database. In this experiment, we will be using interactive mode to manually execute the actual pubSub functionalties.
4. To launch the pubSub client application, run this command: Python -i pub_sub.py [host_ip] [connection_port] [text_file_name]
5. A "socket create successfully" message should be display on the command prompt to indicate the established TCP connection status.

Test command:
Sample command can be retrieve by typing command "api()". The following list specify the avaliable commands includes the required parameters.

pubSub.publish(topic, msg)
pubSub.subscribe(topic,callback_function)
pubSub.unsubscribe(topic)
pubSub.listSubscribedTopics()
pubSub.advertise(topic)



